@ECHO OFF

IF NOT EXIST go.mod (
  ECHO Missing go.mod file
  PAUSE
  EXIT
)

SET /p os="Enter target OS: "
SET /p arch="Enter target arch: "

SET GOOS=%os%
SET GOARCH=%arch%
SET BUILDNAME=ugxtool
SET BUILDSPATH=build\%os%\%arch%

if %arch%==arm (
  SET /p armv="Enter target ARM version: "
  SET GOARM=%armv%
)

if %arch%==arm (
  SET BUILDSPATH=%BUILDSPATH%\v%armv%
)

IF %os%==windows (
  IF NOT %arch%==arm (
    SET BUILDNAME=ugxtool.exe
    CHOICE /C YN /N /M "Use win32 resources? [Y/N]: "
    IF ERRORLEVEL 2 GOTO RESOURCELESS
    IF ERRORLEVEL 1 GOTO RESOURCE
    ECHO.
    GOTO START
  )
)

:RESOURCELESS
ECHO.
ECHO Building UGXTool...
go build -ldflags "-s -w" -o %BUILDSPATH%\%BUILDNAME%
GOTO END

:RESOURCE
ECHO.
ECHO Building UGXTool with win32 resources...
SET GO111MODULE=off
go get github.com/josephspurrier/goversioninfo/cmd/goversioninfo
SET GO111MODULE=on
go generate
go build -ldflags "-s -w" -o %BUILDSPATH%\%BUILDNAME%
if exist resource.syso del resource.syso
GOTO END

:END
ECHO.
ECHO Done.
ECHO.
PAUSE
EXIT
