# UGXTool

UGXTool is a CLI app for Lineage 2 Scaleform based unreal packages.

## Requirements for building

- [Go 1.16](https://golang.org/dl/)
- [goversioninfo](https://github.com/josephspurrier/goversioninfo)

## Build

UGXTool uses goversioninfo for adding additional resources to a win32 executable. The included build.bat will download the required goversioninfo dependencies. You can also run the next command to get the dependencies manually:

> go get https://github.com/josephspurrier/goversioninfo

Execute build.bat for a streamlined build process.

Example:
>- Target OS: windows
>- Target arch: 386 (32bit) or amd64 (64bit)
>- Use win32 resources: Y

More info about golang build command [here](https://golang.org/cmd/go/#hdr-Compile_packages_and_dependencies)

## Usage

```shell
ugxtool [command]

Available Commands:
  build       Build a new L2 UGX Scaleform package
  export      Export the contents of a L2 Scaleform UGX package
  list        List the contents of a L2 Scaleform UGX package

Flags:
  -h, --help      help for ugxtool
  -v, --version   version for ugxtool

Use "ugxtool [command] --help" for more information about a command.
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
