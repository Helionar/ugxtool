package cmd

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/ftrvxmtrx/tga"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gitlab.com/Helionar/ugxtool/pkg/swf"
	"gitlab.com/Helionar/ugxtool/pkg/unreal"
)

var expSwfFlag string
var expImgFlag string

func init() {
	exportCmd.Flags().StringVarP(&expSwfFlag, "swf", "s", "", "save found CFX files as SWF (fws, cws)")
	exportCmd.Flags().Lookup("swf").NoOptDefVal = "cws"
	exportCmd.Flags().StringVarP(&expImgFlag, "img", "i", "", "save found Uncompressed RGBA as Image (tga)")
	exportCmd.Flags().Lookup("img").NoOptDefVal = "tga"
}

var exportCmd = &cobra.Command{
	Use:   "export <ugx file>",
	Short: `Export the contents of a L2 Scaleform UGX package`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Missing UGX file\n")
		}
		if len(args) > 1 {
			return errors.New("Too many arguments\n")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {

		path := args[0]

		file, err := os.Open(path)
		check(err)
		fStats, err := file.Stat()
		check(err)

		defer file.Close()

		fileName := fStats.Name()
		trimFileName := strings.TrimSuffix(fileName, filepath.Ext(fileName))

		if filepath.Ext(fileName) != ".ugx" {
			fmt.Println("Invalid UGX")
			os.Exit(1)
		}

		baseDir, err := exists("PackageExports" + "\\" + trimFileName)
		if baseDir == false {
			err = os.MkdirAll("PackageExports"+"\\"+trimFileName, 0755)
			check(err)
		}

		// Header parsed data
		data := unreal.ReadNextBytes(file, unreal.HeaderSize, 0)
		header, err := unreal.PackageHeaderParser(data)
		check(err)

		// ExportTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.ExportOffset))
		exports, err := unreal.ExportTableParser(header.ExportCount, data)
		check(err)

		// ImportTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.ImportOffset))
		imports, err := unreal.ImportTableParser(header.ImportCount, data)
		check(err)

		// NameTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.NameOffset))
		names, err := unreal.NameTableParser(header.NameCount, data)
		check(err)

		fmt.Printf("\n ■ Package: %s\n", filepath.Base(path))

		tHeader := table.NewWriter()
		tHeader.SetOutputMirror(os.Stdout)
		tHeader.AppendHeader(table.Row{
			"File Version",
			"Licensee Version",
			"Names",
			"Names",
			"Imports",
		})
		tHeader.AppendRow(table.Row{
			header.PackageVersion,
			header.LicenseeVersion,
			header.NameCount,
			header.ExportCount,
			header.ImportCount,
		})
		tHeader.SetStyle(table.StyleLight)
		tHeader.Render()
		fmt.Printf(" GUID: %s\n", header.GUID.String())

		fmt.Printf("\n ■ Exporting contents from package %s...\n", fStats.Name())

		tExps := table.NewWriter()
		tExps.SetOutputMirror(os.Stdout)
		tExps.AppendHeader(table.Row{
			"#",
			"Class",
			"Name",
			"Type",
			"Size (B)",
			"Status",
		})

		swfi := 0
		tSwf := table.NewWriter()
		tSwf.SetOutputMirror(os.Stdout)
		tSwf.AppendHeader(table.Row{
			"#",
			"Name",
			"Format",
			"Ver",
			"Size (B)",
			"Ori Size",
			"Status",
		})

		imgi := 0
		tImg := table.NewWriter()
		tImg.SetOutputMirror(os.Stdout)
		tImg.AppendHeader(table.Row{
			"#",
			"Name",
			"Format",
			"Width",
			"Height",
			"Size (B)",
			"Status",
		})

		for i, exp := range exports {

			classIdx := unreal.GetExternalIndex(exp.ClassIndex)
			var extName int

			if exp.ClassIndex < 0 {
				extName = imports[classIdx].ObjectName
			} else if exp.ClassIndex > 0 {
				extName = exports[classIdx].ObjectName
			}

			name := names[exp.ObjectName].ObjectName.NameString
			objName := names[extName].ObjectName.NameString

			classDir, err := exists("PackageExports" + "\\" + trimFileName + "\\" + objName)
			if classDir == false {
				err = os.MkdirAll("PackageExports"+"\\"+trimFileName+"\\"+objName, 0755)
				check(err)
			}

			outFile, err := os.Create("PackageExports" + "\\" + trimFileName + "\\" + objName + "\\" + name + "." + objName)
			check(err)
			defer outFile.Close()

			binData := unreal.ReadNextBytes(file, exp.SerialSize, exp.SerialOffset)
			reader := bytes.NewReader(binData)
			_, err = outFile.Write(binData)

			// Unknown first value. Always 0x00
			unreal.ReadCompactIndex(reader)
			_, expIdx := unreal.ReadCompactIndex(reader)
			signature := []byte{0x00}

			objType := names[expIdx].ObjectName.NameString

			switch objType {
			case "tga":
				signature = []byte(strings.ToUpper(objType))
				if expImgFlag == "tga" {
					classDir, err := exists("PackageExports" + "\\" + trimFileName + "\\" + "TGA")
					if classDir == false {
						err = os.MkdirAll("PackageExports"+"\\"+trimFileName+"\\"+"TGA", 0755)
						check(err)
					}
					tgaFile, err := os.Create("PackageExports" + "\\" + trimFileName + "\\" + "TGA" + "\\" + name + ".tga")
					check(err)

					defer tgaFile.Close()

					length, _ := unreal.ReadCompactIndex(reader)
					unrealHdrLen := 2 + length
					_, err = tgaFile.Write(binData[unrealHdrLen:])
					check(err)

					tgaCfg, err := tga.DecodeConfig(reader)
					check(err)

					tImg.AppendRow(table.Row{imgi, name, fmt.Sprintf("%s", signature), tgaCfg.Width, tgaCfg.Height, len(binData[unrealHdrLen:]), "\033[32mOK\033[0m"})
					imgi += 1
				}
			case "gfx":
				signature = []byte(strings.ToUpper(objType))
				if expSwfFlag == "fws" || expSwfFlag == "cws" {
					classDir, err := exists("PackageExports" + "\\" + trimFileName + "\\" + "SWF")
					if classDir == false {
						err = os.MkdirAll("PackageExports"+"\\"+trimFileName+"\\"+"SWF", 0755)
						check(err)
					}
					swfFile, err := os.Create("PackageExports" + "\\" + trimFileName + "\\" + "SWF" + "\\" + name + ".swf")
					check(err)

					defer swfFile.Close()

					length, _ := unreal.ReadCompactIndex(reader)

					unrealHdrLen := 2 + length
					binData[unrealHdrLen+1] = 0x57
					binData[unrealHdrLen+2] = 0x53

					parser := swf.NewParser(reader, unrealHdrLen)
					swfData, err := parser.Parse()
					check(err)

					signLen := 3
					outSign := signature
					if expSwfFlag == "fws" {
						outSign = []byte("FWS")
					} else if expSwfFlag == "cws" {
						outSign = []byte("CWS")
					}
					_, err = swfFile.Write(outSign)
					check(err)

					verlen, err := swfFile.Write([]byte{swfData.Header.Version})
					check(err)

					fwsLen := make([]byte, 4)
					binary.LittleEndian.PutUint32(fwsLen, swfData.Header.FileLength)
					_, err = swfFile.Write(fwsLen)
					check(err)

					swfOfsLen := unrealHdrLen + signLen + verlen + len(fwsLen)

					if expSwfFlag == "cws" {
						_, err = swfFile.Write(binData[swfOfsLen:])
						check(err)
					} else if expSwfFlag == "fws" {
						b := bytes.NewReader(binData[swfOfsLen:])
						zLibDec, err := zlib.NewReader(b)
						check(err)
						io.Copy(swfFile, zLibDec)
					}

					tSwf.AppendRow(table.Row{swfi, name, fmt.Sprintf("SWF (%s)", outSign), fmt.Sprintf("v%d", swfData.Header.Version), len(binData[unrealHdrLen:]), swfData.Header.FileLength, "\033[32mOK\033[0m"})
					swfi += 1
				}
			}
			check(err)

			tExps.AppendRow(table.Row{i, objName, name, fmt.Sprintf("%s", signature), exp.SerialSize, "\033[32mOK\033[0m"})
		}

		dataFile, err := os.Create("PackageExports" + "\\" + trimFileName + "\\" + "PackageData.json")
		check(err)
		defer dataFile.Close()

		pkgData := unreal.PackageData{}

		pkgData.Header = header
		pkgData.NameTable = names
		pkgData.ImportTable = imports
		pkgData.ExportTable = exports

		pkgJson, err := json.MarshalIndent(pkgData, "", "\t")
		check(err)
		_, err = dataFile.Write(pkgJson)
		check(err)

		// Render tables
		tExps.SetStyle(table.StyleLight)
		tExps.Render()

		if expSwfFlag != "" {
			tSwf.SetStyle(table.StyleLight)
			fmt.Printf("\n ■ SWF\n")
			tSwf.Render()
		}

		if expImgFlag != "" {
			tImg.SetStyle(table.StyleLight)
			fmt.Printf("\n ■ Images\n")
			tImg.Render()
		}
	},
}
