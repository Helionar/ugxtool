package cmd

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/Helionar/ugxtool/pkg/unreal"
)

var buildSwfFlag bool
var buildImgFlag bool

func init() {
	buildCmd.Flags().BoolVarP(&buildSwfFlag, "swf", "s", false, "build using SWF/CWS files instead of GFxFlash objects")
	buildCmd.Flags().Lookup("swf").NoOptDefVal = "true"
	buildCmd.Flags().BoolVarP(&buildImgFlag, "img", "i", false, "build using TGA files instead of GFxFlash objects")
	buildCmd.Flags().Lookup("img").NoOptDefVal = "true"
}

var buildCmd = &cobra.Command{
	Use:   "build <PackageData json> <UGX name>",
	Short: "Build a new L2 UGX Scaleform package",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Missing PackageData json\n")
		}
		if len(args) == 1 {
			return errors.New("Missing result package name\n")
		}
		if len(args) > 2 {
			return errors.New("Too many arguments\n")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		path := args[0]
		outName := args[1]

		jsonFile, err := os.Open(path)
		check(err)
		defer jsonFile.Close()

		fmt.Println("Successfully Opened PackageData.json")
		byteValue, _ := ioutil.ReadAll(jsonFile)

		var pkgData unreal.PackageData

		json.Unmarshal(byteValue, &pkgData)

		baseDir, err := exists("PackageBuilds")
		if baseDir == false {
			err = os.Mkdir("PackageBuilds", 0755)
			check(err)
		}

		outFile, err := os.Create("PackageBuilds" + "\\" + outName)
		check(err)
		defer outFile.Close()

		// Write Header
		err = binary.Write(outFile, binary.LittleEndian, pkgData.Header)
		check(err)

		// Write NameTable
		nameOfs := make([]byte, 4)
		binary.LittleEndian.PutUint32(nameOfs, uint32(unreal.HeaderSize))
		nameCnt := make([]byte, 4)
		binary.LittleEndian.PutUint32(nameCnt, uint32(len(pkgData.NameTable)))
		nameTableLen := 0
		for _, name := range pkgData.NameTable {
			err = binary.Write(outFile, binary.LittleEndian, name.ObjectName.NameLength)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, []byte(name.ObjectName.NameString))
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, []byte{0x00})
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, name.ObjectFlags)
			check(err)
			nameTableLen += len(name.ObjectName.NameString) + 6
		}

		// Write Objects
		tmpObjOfs := 0
		for i, exp := range pkgData.ExportTable {
			classidx := unreal.GetExternalIndex(exp.ClassIndex)
			var extName int

			if exp.ClassIndex < 0 {
				extName = pkgData.ImportTable[classidx].ObjectName
			} else if exp.ClassIndex > 0 {
				extName = pkgData.ExportTable[classidx].ObjectName
			}

			name := pkgData.NameTable[exp.ObjectName].ObjectName.NameString
			objName := pkgData.NameTable[extName].ObjectName.NameString

			dir, _ := filepath.Split(path)
			objPath := dir + "\\" + objName + "\\" + name + "." + objName
			swfPath := dir + "SWF" + "\\" + name + "." + "swf"
			imgPath := dir + "TGA" + "\\" + name + "." + "tga"

			swfMatch, err := filepath.Glob(swfPath)
			check(err)
			imgMatch, err := filepath.Glob(imgPath)
			check(err)

			var gfxIdx int
			var tgaIdx int
			var objNameIdx int
			for i, name := range pkgData.NameTable {
				// gfx and TGA references
				if name.ObjectName.NameString == "gfx" {
					gfxIdx = i
				} else if name.ObjectName.NameString == "tga" {
					tgaIdx = i
				}
			}

			if buildSwfFlag && len(swfMatch) > 0 {
				objPath = swfPath
				objNameIdx = gfxIdx
				// fmt.Println(objNameIdx)
			} else if buildImgFlag && len(imgMatch) > 0 {
				objPath = imgPath
				objNameIdx = tgaIdx
				// fmt.Println(objNameIdx)
			}

			expFile, err := os.Open(objPath)
			check(err)
			defer expFile.Close()

			objStats, err := expFile.Stat()
			check(err)

			var fileLen int64
			buf := make([]byte, objStats.Size())

			initialByteLen := 0
			objNameIdxLen := 0
			cIndexLen := 0
			if (buildSwfFlag && len(swfMatch) > 0) || (buildImgFlag && len(imgMatch) > 0) {
				initialByteLen, err = outFile.Write([]byte{0x00})
				check(err)
				objNameIdxLen, err = outFile.Write(unreal.IntToCompactIndex(objNameIdx))
				check(err)
				cIndexLen, err = outFile.Write(unreal.IntToCompactIndex(int(objStats.Size())))
				check(err)

				formatHeader := unreal.ReadNextBytes(expFile, 3, 0)
				if bytes.Compare(formatHeader, []byte("FWS")) == 0 {
					formatHeader = []byte("GFX")
				} else if bytes.Compare(formatHeader, []byte("CWS")) == 0 {
					formatHeader = []byte("CFX")
				}
				_, err = outFile.Write(formatHeader)
				check(err)

				fmt.Println(name + " found! Adding to package...")
			}

			extraBytesLen := int(int64(initialByteLen) + int64(objNameIdxLen) + int64(cIndexLen))

			fileLen, err = io.CopyBuffer(outFile, expFile, buf)
			check(err)

			if (buildSwfFlag && len(swfMatch) > 0) || (buildImgFlag && len(imgMatch) > 0) {
				fileLen += 3 // formatHeader
			}

			pkgData.ExportTable[i].SerialSize = len(buf) + extraBytesLen
			pkgData.ExportTable[i].SerialOffset = 64 + nameTableLen + tmpObjOfs

			tmpObjOfs += int(fileLen) + extraBytesLen

			// println(pkgData.ExportTable[i].SerialSize)
			// println(pkgData.ExportTable[i].SerialOffset)
			// println("-")
		}

		// Write ImportTable
		impOfs := make([]byte, 4)
		binary.LittleEndian.PutUint32(impOfs, uint32(64+nameTableLen+tmpObjOfs))
		impCnt := make([]byte, 4)
		binary.LittleEndian.PutUint32(impCnt, uint32(len(pkgData.ImportTable)))
		impTableLen := 0
		for _, imp := range pkgData.ImportTable {
			classPkg := unreal.IntToCompactIndex(imp.ClassPackage)
			className := unreal.IntToCompactIndex(imp.ClassName)
			objName := unreal.IntToCompactIndex(imp.ObjectName)
			err = binary.Write(outFile, binary.LittleEndian, classPkg)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, className)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, imp.PackageIndex)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, objName)
			check(err)
			impTableLen += len(classPkg) + len(className) + len(objName) + 4
		}

		// Write ExportTable
		expOfs := make([]byte, 4)
		binary.LittleEndian.PutUint32(expOfs, uint32(64+nameTableLen+tmpObjOfs+impTableLen))
		expCnt := make([]byte, 4)
		binary.LittleEndian.PutUint32(expCnt, uint32(len(pkgData.ExportTable)))
		for _, exp := range pkgData.ExportTable {
			err = binary.Write(outFile, binary.LittleEndian, unreal.IntToCompactIndex(exp.ClassIndex))
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, unreal.IntToCompactIndex(exp.SuperIndex))
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, exp.PackageIndex)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, unreal.IntToCompactIndex(exp.ObjectName))
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, exp.ObjectFlags)
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, unreal.IntToCompactIndex(exp.SerialSize))
			check(err)
			err = binary.Write(outFile, binary.LittleEndian, unreal.IntToCompactIndex(exp.SerialOffset))
			check(err)
		}

		err = unreal.SetNameCount(outFile, nameCnt)
		check(err)
		err = unreal.SetNameOffset(outFile, nameOfs)
		check(err)
		err = unreal.SetExportCount(outFile, expCnt)
		check(err)
		err = unreal.SetExportOffset(outFile, expOfs)
		check(err)
		err = unreal.SetImportCount(outFile, impCnt)
		check(err)
		err = unreal.SetImportOffset(outFile, impOfs)
		check(err)
		err = unreal.SetGenerations(outFile, int(pkgData.Header.GenerationCount), expCnt, nameCnt)
		check(err)

		fmt.Println("Successfully Built " + outName + " package")
	},
}
