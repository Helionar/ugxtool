package cmd

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gitlab.com/Helionar/ugxtool/pkg/unreal"
)

var listCmd = &cobra.Command{
	Use:   "list <ugx file>",
	Short: `List the contents of a L2 Scaleform UGX package`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Missing UGX file\n")
		}
		if len(args) > 1 {
			return errors.New("Too many arguments\n")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {

		path := args[0]

		file, err := os.Open(path)
		check(err)
		fStats, err := file.Stat()
		check(err)

		defer file.Close()

		fileName := fStats.Name()

		if filepath.Ext(fileName) != ".ugx" {
			fmt.Println("Invalid UGX")
			os.Exit(1)
		}

		// Header parsed data
		data := unreal.ReadNextBytes(file, unreal.HeaderSize, 0)
		header, err := unreal.PackageHeaderParser(data)
		check(err)

		// ExportTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.ExportOffset))
		exports, err := unreal.ExportTableParser(header.ExportCount, data)
		check(err)

		// ImportTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.ImportOffset))
		imports, err := unreal.ImportTableParser(header.ImportCount, data)
		check(err)

		// NameTable parsed data
		data = unreal.ReadNextBytes(file, int(fStats.Size()), int(header.NameOffset))
		names, err := unreal.NameTableParser(header.NameCount, data)
		check(err)

		fmt.Printf("\n ■ Package: %s\n", filepath.Base(path))

		tHeader := table.NewWriter()
		tHeader.SetOutputMirror(os.Stdout)
		tHeader.AppendHeader(table.Row{
			"File Version",
			"Licensee Version",
			"Names",
			"Names",
			"Imports",
		})
		tHeader.AppendRow(table.Row{
			header.PackageVersion,
			header.LicenseeVersion,
			header.NameCount,
			header.ExportCount,
			header.ImportCount,
		})
		tHeader.SetStyle(table.StyleLight)
		tHeader.Render()
		fmt.Printf(" GUID: %s\n", header.GUID.String())

		// List Imports
		fmt.Printf("\n ■ Package Imports\n")

		tImps := table.NewWriter()
		tImps.SetOutputMirror(os.Stdout)
		tImps.AppendHeader(table.Row{
			"#",
			"Class Pkg",
			"Class Name",
			"Pkg Index",
			"Obj Name",
		})

		for i, imp := range imports {

			classPkg := names[imp.ClassPackage].ObjectName.NameString
			className := names[imp.ClassName].ObjectName.NameString
			objName := names[imp.ObjectName].ObjectName.NameString

			pkgIdx := ""
			selfImpIdx := unreal.GetExternalIndex(int(imp.PackageIndex))
			if selfImpIdx != 0 {
				pkgIdx = fmt.Sprintf(
					"[%d] %s",
					selfImpIdx,
					names[imports[selfImpIdx].ObjectName].ObjectName.NameString,
				)
			}

			tImps.AppendRow(table.Row{
				i,
				classPkg,
				className,
				pkgIdx,
				objName,
			})
		}
		tImps.SetStyle(table.StyleLight)
		tImps.Render()

		// List Exports
		fmt.Printf("\n ■ Package Exports\n")

		tExps := table.NewWriter()
		tExps.SetOutputMirror(os.Stdout)
		tExps.AppendHeader(table.Row{
			"#",
			"Class",
			"Name",
			"Type",
			"Size (B)",
			"Offset",
			"Obj Flags",
		})

		for i, exp := range exports {

			classIdx := unreal.GetExternalIndex(exp.ClassIndex)
			var extName int

			if exp.ClassIndex < 0 {
				extName = imports[classIdx].ObjectName
			} else if exp.ClassIndex > 0 {
				extName = exports[classIdx].ObjectName
			}

			name := names[exp.ObjectName].ObjectName.NameString
			objName := names[extName].ObjectName.NameString

			binData := unreal.ReadNextBytes(file, exp.SerialSize, exp.SerialOffset)
			reader := bytes.NewReader(binData)

			// Unknown first value. Always 0x00
			unreal.ReadCompactIndex(reader)

			// GFxFlash Type
			_, expIndex := unreal.ReadCompactIndex(reader)
			typeStr := "Unknown"

			objType := names[expIndex].ObjectName.NameString

			switch objType {
			case "tga", "gfx":
				typeStr = fmt.Sprintf(strings.ToUpper(objType))
			}

			tExps.AppendRow(table.Row{
				i,
				objName,
				name,
				typeStr,
				exp.SerialSize,
				fmt.Sprintf("0x%08X", exp.SerialOffset),
				fmt.Sprintf("0x%08X", exp.ObjectFlags),
			})
		}
		tExps.SetStyle(table.StyleLight)
		tExps.Render()
	},
}
