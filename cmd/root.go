package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(listCmd)
	rootCmd.AddCommand(exportCmd)
	rootCmd.AddCommand(buildCmd)
}

var (
	rootCmd = &cobra.Command{
		Use:   "ugxtool",
		Short: "L2 UGXTool for Scaleform based packages",
		Long: `
UGXTool is a CLI app for L2 Scaleform based packages.
This application can export tables and objects contained
in a L2 Scaleform UGX package, decompile GFxFlash files
and insert them back.`,
		Version: "v1.0.0.1",
	}
)

func Execute() {
	rootCmd.SetHelpCommand(&cobra.Command{Hidden: true})
	rootCmd.Execute()
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func check(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
