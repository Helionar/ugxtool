package unreal

import "github.com/pborman/dsp0134"

// =======================
// UNREAL DATA STRUCTS //
// =============================================================================

type Header struct {
	Signature       uint32
	PackageVersion  uint16
	LicenseeVersion uint16
	PackageFlags    uint32
	NameCount       uint32
	NameOffset      uint32
	ExportCount     uint32
	ExportOffset    uint32
	ImportCount     uint32
	ImportOffset    uint32
	GUID            dsp0134.UUID //DSP0134 standard
	GenerationCount uint32
	GenerationInfos struct {
		ExportCount uint32
		NameCount   uint32
	}
}

type Name struct {
	ObjectName struct {
		NameLength uint8
		NameString string
	}
	ObjectFlags uint32
}

type Import struct {
	ClassPackage int //Compactindex
	ClassName    int //Compactindex
	PackageIndex int32
	ObjectName   int //Compactindex
}

type Export struct {
	ClassIndex   int //Compactindex
	SuperIndex   int //Compactindex
	PackageIndex int32
	ObjectName   int //Compactindex
	ObjectFlags  uint32
	SerialSize   int //Compactindex
	SerialOffset int //Compactindex
}

type PackageData struct {
	Header      Header
	NameTable   []Name
	ImportTable []Import
	ExportTable []Export
}
