package unreal

import (
	"encoding/binary"
	"os"
)

// ===========
// SETTERS //
// =============================================================================

func SetNameCount(file *os.File, nameCnt []byte) error {
	_, err := file.WriteAt(nameCnt, int64(NameCountOfs))
	return err
}

func SetNameOffset(file *os.File, nameOfs []byte) error {
	_, err := file.WriteAt(nameOfs, int64(NameOffsetOfs))
	return err
}

func SetImportCount(file *os.File, impCnt []byte) error {
	_, err := file.WriteAt(impCnt, int64(ImportCountOfs))
	return err
}

func SetImportOffset(file *os.File, impOfs []byte) error {
	_, err := file.WriteAt(impOfs, int64(ImportOffsetOfs))
	return err
}

func SetExportCount(file *os.File, expCnt []byte) error {
	_, err := file.WriteAt(expCnt, int64(ExportCountOfs))
	return err
}

func SetExportOffset(file *os.File, expOfs []byte) error {
	_, err := file.WriteAt(expOfs, int64(ExportOffsetOfs))
	return err
}

func SetGenerations(file *os.File, genCnt int, expCnt []byte, nameCnt []byte) error {
	genCntB := make([]byte, 4)
	binary.LittleEndian.PutUint32(genCntB, uint32(genCnt))
	_, err := file.WriteAt(genCntB, int64(GenerationsOfs))
	if err != nil {
		return err
	}
	_, err = file.WriteAt(expCnt, int64(GenerationsOfs+0x04))
	if err != nil {
		return err
	}
	_, err = file.WriteAt(nameCnt, int64(GenerationsOfs+0x08))
	return err
}
