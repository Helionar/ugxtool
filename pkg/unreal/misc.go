package unreal

import (
	"log"
	"os"
	"unsafe"
)

// ========
// MISC //
// =============================================================================

// Read the next bytes with additional delim and offset options
// Params: file *os.File, delim int, offset int
func ReadNextBytes(file *os.File, params ...int) []byte {

	delim := 1
	offset := 0

	for i, opt := range params {
		if i == 0 {
			delim = opt
		} else if i == 1 {
			offset = opt
		}
	}

	if offset > 0 {
		file.Seek(int64(offset), 0)
	}

	bytes := make([]byte, delim)

	_, err := file.Read(bytes)
	if err != nil {
		log.Fatal(err)
	}

	return bytes
}

// Transforms a int64 to a byte array given a delim
func IntToByteArray(num int64, length int) []byte {
	arr := make([]byte, length)
	for i := 0; i < length; i++ {
		byt := *(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&num)) + uintptr(i)))
		arr[i] = byt
	}
	return arr
}

// Int absolute value
func abs(n int) int {
	y := n >> 63
	return (n ^ y) - y
}
