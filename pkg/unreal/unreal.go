package unreal

import (
	"bytes"
	"encoding/binary"
	"errors"
	"io"
	"log"
)

// =======================
// UNREAL HEADER VALUES //
// =============================================================================

var (
	HeaderSize = 64
	signature  = []byte{0xC1, 0x83, 0x2A, 0x9E}

	SignatureOfs    = 0x00
	VersionOfs      = 0x04
	PkgFlagsOfs     = 0x08
	NameCountOfs    = 0x0c
	NameOffsetOfs   = 0x10
	ExportCountOfs  = 0x14
	ExportOffsetOfs = 0x18
	ImportCountOfs  = 0x1C
	ImportOffsetOfs = 0x20
	GUIDOfs         = 0x24
	GenerationsOfs  = 0x34
)

// =================
// COMPACT INDEX // Custom Unreal Data Type
// =============================================================================

// CompactIndex is a special integer data type used very often in the Unreal
// Engine. It is used to reduce the size of a integer. However in the worst
// case scenario it takes up one extra byte (5bytes instead of 4), but this
// isn't often the case. CompactIndex is for example used to store the length
// of a string.

// A CompactIndex tries to use as less bytes as possible to store the
// integer's (32bit) value.

// *1st Byte*
// The MSB (Most Significant Bit) flags if the string is in UTF-16 encoding
// instead of normal ASCII. Thus the MSB only has a meaning when used for
// string length. MSB-1 defines if there's another byte following this byte
// that should be included with the CompactIndex. The other 6 bits are used
// for the value. Therefor the maximum value in the first byte is (2^6)-1 = 63

// *2nd to 4th Byte*
// The MSB defines if there's another byte following this byte that should be
// included with the CompactIndex. The rest is used for the value. (2^7)-1 = 127

// *5th Byte*
// The whole byte is used for the value. However, because the first 4 bytes
// already consume 27 of the 32 bits only 5 bits are used in this byte.

// References:
// https://beyondunrealwiki.github.io/pages/package-file-format-data-de.html
// https://beyondunrealwiki.github.io/pages/compactindex.html

// =============================================================================

// Reads byte from a buffer and returns an uncompacted index as int
// Due the variable length of a CompactIndex, this function moves forward
// the pointer of the passed reader
func ReadCompactIndex(reader *bytes.Reader) (int, int) {

	var output int
	var signed bool
	var length int

	for i := 0; i < 5; i++ {
		x, err := reader.ReadByte()
		if err == io.EOF {
			log.Fatal("unreal.ReadCompactIndex -> Invalid CompactIndex value")
		}

		n := int(x)

		if i == 0 {
			// Bit: X0000000
			if (x & 0x80) > 0 {
				signed = true
			}
			// Bits: 00XXXXXX
			output |= (n & 0x3F)
			// Bit: 0X000000
			if (x & 0x40) == 0 {
				length = 1
				break
			}
		} else if i == 4 {
			// Bits: 000XXXXX -- the 0 bits are ignored
			// (hits the 32 bit boundary)
			output |= (n & 0x1F) << (6 + (3 * 7))
			length = 5
		} else {
			// Bits: 0XXXXXXX
			output |= (n & 0x7F) << (6 + ((i - 1) * 7))
			// Bit: X0000000
			if (x & 0x80) == 0 {
				length = i + 1
				break
			}
		}
	}

	if signed {
		output = -output
	}

	return length, output
}

// Read an int and returns a Compact Index as byte array
func IntToCompactIndex(integer int) []byte {

	var val int
	var cindex [5]int
	var output []byte
	var pointer int

	val = abs(integer)
	if val != integer {
		cindex[pointer] = 0x80 // B1 sign bit
	}

	cindex[pointer] += val & 0x3f // B1 data, 6 bits
	val = val >> 6
	if val != 0 {
		cindex[pointer] += 0x40 // B1 continue bit
		pointer += 1
		cindex[pointer] = val & 0x7f // B2 data, 7 bits
		val = val >> 7
		if val != 0 {
			cindex[pointer] += 0x80 // B2 continue bit
			pointer += 1
			cindex[pointer] = val & 0x7f // B3 data, 7 bits
			val = val >> 7
			if val != 0 {
				cindex[pointer] += 0x80 // B3 continue bit
				pointer += 1
				cindex[pointer] = val & 0x7f // B4 data, 7 bits
				val = val >> 7
				if val != 0 {
					cindex[pointer] += 0x80 // B4 continue bit
					pointer += 1
					cindex[pointer] = val & 0x1f // B5 data, 5 bits
				}
			}
		}
	}

	length := pointer + 1

	for i := 0; i < length; i++ {
		output = append(output, uint8(cindex[i]))
	}

	return output
}

// ========
// NAME // Custom Unreal Data Type
// =============================================================================

// The name table specifies name values used throughout the package. The size
// and position of the name table is specified in the package header. Its
// entries are structured as follows:

// Version	Type	Name		Description
// 			STRING	Name str	The string this name represents.
// <141		DWORD	Name flags	Flags associated with this name.
// >=141	QWORD	Name flags	Flags associated with this name.

// Generally the name table does not contain any duplicate entries, including no
// different capitalizations. The only exception may be the name 'None', which
// may be occur several times if the package was conformed to a previous version.
// Names in the name table are grouped by the generation they were added in.

// Every generation's name group starts with a new 'None' name. The indices of
// additional 'None' names correspond to the name counts of previous generations
// in the package header's generation info list.

// References:
// https://wiki.beyondunreal.com/Unreal_package

// =============================================================================

// Reads byte from a buffer and returns a name string
// Due the variable length of a Name, this function moves forward the pointer
// of the passed reader
func ReadName(reader *bytes.Reader) (uint8, string) {

	var output string

	length, err := reader.ReadByte()
	objname := make([]byte, length)
	_, err = reader.Read(objname)

	if err == io.EOF {
		log.Fatal("unreal.ReadBytesName -> Invalid Name value")
	}

	output = string(bytes.Trim(objname, "\x00"))

	return length, output
}

// ================
// DATA PARSERS //
// =============================================================================

// TODO: Error handling

// =============================================================================

func PackageHeaderParser(data []byte) (Header, error) {

	// Errors
	var err error
	sigLen := errors.New("Invalid UGX")
	sigMismatch := errors.New("Invalid UGX: Signature mismatch")

	header := Header{}

	//Check Header Length
	if len(data) != HeaderSize {
		err = sigLen
		goto Skip
	}

	//Check Unreal Signature
	if bytes.Compare(data[0:4], signature) != 0 {
		err = sigMismatch
		goto Skip
	}

	if err == nil {
		buffer := bytes.NewBuffer(data)
		err = binary.Read(buffer, binary.LittleEndian, &header)
	}

Skip:
	return header, err

}

func NameTableParser(count uint32, data []byte) ([]Name, error) {

	names := []Name{}
	reader := bytes.NewReader(data)

	for i := 0; i < int(count); i++ {

		length, objname := ReadName(reader)
		oflg := make([]byte, 4)
		reader.Read(oflg)

		name := Name{}
		name.ObjectName.NameLength = uint8(length)
		name.ObjectName.NameString = objname
		name.ObjectFlags = binary.LittleEndian.Uint32(oflg)
		names = append(names, name)
	}

	// TODO: Error handling
	return names, nil
}

func ExportTableParser(count uint32, data []byte) ([]Export, error) {

	exports := []Export{}

	reader := bytes.NewReader(data)

	for i := 0; i < int(count); i++ {

		export := Export{}

		_, cIdx := ReadCompactIndex(reader)
		_, sIdx := ReadCompactIndex(reader)
		pkgIdx := make([]byte, 4)
		reader.Read(pkgIdx)
		_, objName := ReadCompactIndex(reader)
		objFlag := make([]byte, 4)
		reader.Read(objFlag)
		_, serialSize := ReadCompactIndex(reader)
		_, serialOfs := ReadCompactIndex(reader)

		export.ClassIndex = cIdx
		export.SuperIndex = sIdx
		export.PackageIndex = int32(binary.LittleEndian.Uint32(pkgIdx))
		export.ObjectName = objName
		export.ObjectFlags = uint32(binary.LittleEndian.Uint32(objFlag))
		export.SerialSize = serialSize
		export.SerialOffset = serialOfs
		exports = append(exports, export)
	}

	// TODO: Error handling
	return exports, nil
}

func ImportTableParser(count uint32, data []byte) ([]Import, error) {

	imports := []Import{}

	reader := bytes.NewReader(data)

	for i := 0; i < int(count); i++ {

		imp0rt := Import{}

		_, classPkg := ReadCompactIndex(reader)
		_, className := ReadCompactIndex(reader)
		pkgIdx := make([]byte, 4)
		reader.Read(pkgIdx)
		_, objName := ReadCompactIndex(reader)

		imp0rt.ClassPackage = classPkg
		imp0rt.ClassName = className
		imp0rt.PackageIndex = int32(binary.LittleEndian.Uint32(pkgIdx))
		imp0rt.ObjectName = objName
		imports = append(imports, imp0rt)
	}

	// TODO: Error handling
	return imports, nil
}

// ==================
// EXTERNAL INDEX //
// =============================================================================

// Some indices do not refer to a name in the Name Table, but to other objects
// in the Export or Import tables. They work in this way:

// If the index is zero the object referenced is null.
// If the index<0 the object is in the Import table in the position (–index-1).
// If the index>0 the object is in the Export table in the position (index-1).

// =============================================================================

// Get an External Index from a given integer
func GetExternalIndex(index int) int {
	var extIndex int
	if index < 0 {
		extIndex = -index - 1
	} else if index > 0 {
		extIndex = index - 1
	}
	return extIndex
}
