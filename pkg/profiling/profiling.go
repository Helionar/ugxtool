package profiling

import (
	"fmt"
	"runtime"
)

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v bytes", m.Alloc)
	fmt.Printf(" | TotalAlloc = %v bytes", m.TotalAlloc)
	fmt.Printf(" | Sys = %v bytes", m.Sys)
	fmt.Printf(" | NumGC = %v\n", m.NumGC)
}
