module gitlab.com/Helionar/ugxtool

go 1.16

require (
	github.com/ftrvxmtrx/tga v0.0.0-20150524081124-bd8e8d5be13a
	github.com/icza/bitio v0.8.0
	github.com/icza/mighty v0.0.0-20200205104645-c377cb773678 // indirect
	github.com/jedib0t/go-pretty/v6 v6.1.1
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/pborman/dsp0134 v0.0.0-20210209165114-5933e5297577
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20191119060738-e882bf8e40c2 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)
